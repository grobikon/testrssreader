package com.grobikon.testrssreader.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.grobikon.testrssreader.common.SessionManager;
import com.grobikon.testrssreader.common.manager.MyFragmentManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Grigoriy Obraztsov on 05.03.2018.
 */

@Module
public class ManagerModule {

    @Singleton
    @Provides
    MyFragmentManager provideMyFragmentManager() {
        return new MyFragmentManager();
    }


    @Singleton
    @Provides
    SessionManager sessionManager(SharedPreferences preferences){
        return new SessionManager(preferences);
    }

    @Singleton
    @Provides
    public SharedPreferences providesSharedPreference(Context context){
        return context.getSharedPreferences(SessionManager.STORAGE_NAME, Context.MODE_PRIVATE);
    }
}
