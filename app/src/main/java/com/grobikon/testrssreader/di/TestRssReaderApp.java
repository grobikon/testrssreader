package com.grobikon.testrssreader.di;

import android.app.Application;
import android.content.Context;

import com.grobikon.testrssreader.di.component.ApplicationComponent;
import com.grobikon.testrssreader.di.component.DaggerApplicationComponent;
import com.grobikon.testrssreader.di.module.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TestRssReaderApp extends Application {
    private static ApplicationComponent sApplicationComponent;

    public static TestRssReaderApp get(Context context){
        return (TestRssReaderApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
        initRealm();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void initComponent(){
        sApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }

    public static ApplicationComponent getApplicationComponent() {
        return sApplicationComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

}
