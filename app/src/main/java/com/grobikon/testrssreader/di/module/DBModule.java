package com.grobikon.testrssreader.di.module;

import android.content.Context;

import com.grobikon.testrssreader.common.db.DBHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DBModule {

    @Provides
    @Singleton
    DBHelper dbHelper(Context context){
        return new DBHelper(context);
    }
}
