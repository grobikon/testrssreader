package com.grobikon.testrssreader.di.component;

import com.grobikon.testrssreader.di.module.ApplicationModule;
import com.grobikon.testrssreader.di.module.DBModule;
import com.grobikon.testrssreader.di.module.ManagerModule;
import com.grobikon.testrssreader.di.module.RealmModule;
import com.grobikon.testrssreader.ui.activity.BaseActivity;
import com.grobikon.testrssreader.ui.activity.TestRssReaderActivity;
import com.grobikon.testrssreader.ui.fragment.RssCurrentFragment;
import com.grobikon.testrssreader.ui.fragment.RssItemFragment;
import com.grobikon.testrssreader.ui.fragment.TestRssReaderFragment;
import com.grobikon.testrssreader.ui.view.holder.RssCurrentItemBodyHolder;
import com.grobikon.testrssreader.ui.view.holder.RssItemBodyHolder;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Grigoriy Obraztsov on 05.03.2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class,
        ManagerModule.class, RealmModule.class, DBModule.class})
public interface ApplicationComponent {
    //ACTIVITY
    void inject(BaseActivity activity);
    void inject(TestRssReaderActivity activity);

    //FRAGMENT
    void inject(RssCurrentFragment fragment);
    void inject(TestRssReaderFragment fragment);
    void inject(RssItemFragment fragment);

    //HOLDER
    void inject(RssItemBodyHolder holder);
    void inject(RssCurrentItemBodyHolder holder);

}
