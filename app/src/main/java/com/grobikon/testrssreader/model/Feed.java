package com.grobikon.testrssreader.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Feed {
    private String url;
    private String title;
    private String link;
    private String author;
    private String description;
    private String image;

    public Feed(String url, String title, String link, String author, String description, String image) {
        this.url = url;
        this.title = title;
        this.link = link;
        this.author = author;
        this.description = description;
        this.image = image;
    }
}
