package com.grobikon.testrssreader.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RSSObject {
    private String status;
    private Feed feed;
    private List<Item> items;
}
