package com.grobikon.testrssreader.model.view;

import android.view.View;

import com.grobikon.testrssreader.common.BaseAdapter;
import com.grobikon.testrssreader.model.RssUrl;
import com.grobikon.testrssreader.ui.view.holder.BaseViewHolder;
import com.grobikon.testrssreader.ui.view.holder.RssCurrentItemBodyHolder;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class RssCurrentItemBodyViewModel extends BaseViewModel{
    private RssUrl rssUrl;
    private BaseAdapter adapter;

    public RssCurrentItemBodyViewModel(RssUrl rssUrl, BaseAdapter adapter) {
        this.rssUrl = rssUrl;
        this.adapter = adapter;
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.RSSCurrentBody;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new RssCurrentItemBodyHolder(view);
    }

    @Override
    public boolean isItemDecorator() {
        return true;
    }

    public BaseAdapter getAdapter() {
        return adapter;
    }

    public RssUrl getRssUrl() {
        return rssUrl;
    }
}
