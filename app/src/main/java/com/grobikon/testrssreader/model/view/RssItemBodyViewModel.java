package com.grobikon.testrssreader.model.view;

import android.view.View;

import com.grobikon.testrssreader.common.BaseAdapter;
import com.grobikon.testrssreader.model.Item;
import com.grobikon.testrssreader.ui.view.holder.BaseViewHolder;
import com.grobikon.testrssreader.ui.view.holder.RssItemBodyHolder;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class RssItemBodyViewModel extends BaseViewModel {
    private Item item;

    public RssItemBodyViewModel(Item item, BaseAdapter adapter) {
        this.item = item;
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.RSSFooter;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new RssItemBodyHolder(view);
    }


    public Item getItem() {
        return item;
    }
}
