package com.grobikon.testrssreader.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class App {
    private String nameApp;
    private String pathApp;
    private int iconRes;

    public App() {
    }
}
