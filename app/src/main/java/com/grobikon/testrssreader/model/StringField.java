package com.grobikon.testrssreader.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StringField {
   private String adj1;
   private String adj2;
   private String noun1;
   private String noun2;

   public StringField() {
   }

   public StringField(String var1, String var2, String var3, String var4) {
      this.adj1 = var1;
      this.adj2 = var2;
      this.noun1 = var3;
      this.noun2 = var4;
   }
}
