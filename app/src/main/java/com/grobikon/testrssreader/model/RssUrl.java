package com.grobikon.testrssreader.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */

@Setter
@Getter
public class RssUrl {
    private int id;
    private String nameRss;
    private String rssUrl;

    public RssUrl() {
    }
}
