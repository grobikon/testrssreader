package com.grobikon.testrssreader.mvp.view;


import com.arellomobile.mvp.MvpView;
import com.grobikon.testrssreader.model.view.BaseViewModel;

import java.util.List;

/**
 * @author Grigoriy Obraztsov on 09.10.2018.
 */
public interface BaseView extends MvpView {
    void showRefreshing();
    void hideRefreshing();
    void showListProgress();
    void hideListProgress();
    void showError(String message);
    void setItems(List<BaseViewModel> items); // замена существующего списка новым
    void addItems(List<BaseViewModel> items); // добавление новых элементов списка в конец существующего
    void showAd();
}
