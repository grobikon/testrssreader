package com.grobikon.testrssreader.ui.view.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.view.RssItemBodyViewModel;
import com.grobikon.testrssreader.ui.activity.BaseActivity;
import com.grobikon.testrssreader.ui.fragment.RssItemFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class RssItemBodyHolder extends BaseViewHolder<RssItemBodyViewModel> {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvPubDate)
    TextView tvPubDate;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @Inject
    Realm realm;

    public RssItemBodyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        TestRssReaderApp.getApplicationComponent().inject(this);
    }

    @Override
    public void bindViewHolder(RssItemBodyViewModel item) {
        Context mCtx = itemView.getContext();
        tvTitle.setText(item.getItem().getTitle());
        tvPubDate.setText(item.getItem().getPubDate());
        tvContent.setText(item.getItem().getContent());

        itemView.setOnClickListener(v -> {
            ((BaseActivity) mCtx).setSelectItem(item.getItem());
            ((BaseActivity) mCtx).addContent(new RssItemFragment());
        });
    }

    @Override
    public void unbindViewHolder() {
        tvTitle.setText(null);
        tvPubDate.setText(null);
        tvContent.setText(null);
    }
}
