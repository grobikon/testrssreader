package com.grobikon.testrssreader.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.grobikon.testrssreader.ConstantsVersion;
import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.androidX.MvpAppCompatActivity;
import com.grobikon.testrssreader.common.SessionManager;
import com.grobikon.testrssreader.common.manager.MyFragmentManager;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.Item;
import com.grobikon.testrssreader.ui.dialog.RateDialogFragment;
import com.grobikon.testrssreader.ui.fragment.BaseFragment;

import javax.inject.Inject;

import androidx.annotation.LayoutRes;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public abstract class BaseActivity extends MvpAppCompatActivity {

    @BindView(R.id.progress)
    protected ProgressBar mProgressBar;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @BindView(R.id.toolbar_common)
    Toolbar toolbar;

    @Inject
    MyFragmentManager myFragmentManager;
    @Inject
    Realm realm;
    @Inject
    SessionManager sm;
    private String version;

    private String uid;

    private Item selectItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);

        TestRssReaderApp.getApplicationComponent().inject(this);
        initVersion();
        setSupportActionBar(toolbar);
        initRate();
    }

    private void initVersion(){
        version = ConstantsVersion.TYPE == ConstantsVersion.Type.ALPHA
                ? "alpha"
                : "product";
    }

    public String getVersion() {
        return version;
    }

    private void initRate(){
        sm.saveCountStarts();
        // Если приложение было запущено 3и раза то показываем диалоговое окно
        // и сбрасываем счетчик
        if (sm.isRate() && sm.getCountStarts() == 3)             //и сбросить счетчик
            RateDialogFragment.showDialog(this, sm);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

    @LayoutRes //метод будет возвращсть ссылку на этот  layout
    protected abstract int getMainContentLayout();

    public void fragmentOnScreen(BaseFragment baseFragment){
        setToolbarTitle(baseFragment.createToolbarTitle(this));
    }

    /**
     * Transition between fragment
     * Adding methods for adding and removing fragments
     */

    // Edit toolbar title
    public void setToolbarTitle(final String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }
    // set root fragment
    public void setContent(final BaseFragment fragment) {
        myFragmentManager.setFragment(this, fragment, R.id.main_wrapper);
    }

    // add new fragment
    public void addContent(final BaseFragment fragment) {
        myFragmentManager.addFragment(this, fragment, R.id.main_wrapper);
    }

    public boolean removeCurrentFragment() {
        return myFragmentManager.removeCurrentFragment(this);
    }

    public boolean removeAllFragment() {
        return myFragmentManager.removeAllFragments(this);
    }

    public boolean removeFragment(final BaseFragment fragment) {
        return myFragmentManager.removeFragment(this, fragment);
    }

    @Override
    public void onBackPressed() {
        // если нажали назад из фрагмента подхода, то сначало проверяем выполнил он его или нет
        removeCurrentFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public Realm getRealm() {
        return realm;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public Item getSelectItem() {
        return selectItem;
    }

    public void setSelectItem(Item selectItem) {
        this.selectItem = selectItem;
    }
}
