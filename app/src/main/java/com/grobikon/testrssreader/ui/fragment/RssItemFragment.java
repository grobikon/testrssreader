package com.grobikon.testrssreader.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.Item;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RssItemFragment extends BaseFragment {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tvPubDate)
    TextView tvPubDate;
    @BindView(R.id.tvContent)
    TextView tvContent;


    public RssItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestRssReaderApp.getApplicationComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initData();
    }

    private void initData() {
        Item item = getBaseActivity().getSelectItem();

        if ( item != null ) {
            tvTitle.setText(item.getTitle());
            tvContent.setText(item.getContent());
            tvPubDate.setText(item.getContent());
            // Кастим obj в JSONObject
            String jo = item.getEnclosure().toString();
            if (jo.length()> 6) {
                // Достаём firstName and lastName
                Glide.with(this)
                        .load(jo.substring(0, jo.lastIndexOf(",")).substring(6))
                        .into(ivImage);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_rss_item;
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.ri_item;
    }
}
