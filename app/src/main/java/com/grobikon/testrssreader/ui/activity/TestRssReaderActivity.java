package com.grobikon.testrssreader.ui.activity;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.ui.fragment.AppsFragment;
import com.grobikon.testrssreader.ui.fragment.RssCurrentFragment;
import com.grobikon.testrssreader.ui.fragment.TestRssReaderFragment;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class TestRssReaderActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContent(new TestRssReaderFragment());
        navigation.setSelectedItemId(R.id.navigation_random);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected int getMainContentLayout() {
        return 0;
    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_apps:
                setContent(new AppsFragment());
                return true;
            case R.id.navigation_random:
                setContent(new TestRssReaderFragment());
                return true;
            case R.id.navigation_like:
                setContent(new RssCurrentFragment());
                return true;
        }
        return false;
    };
}
