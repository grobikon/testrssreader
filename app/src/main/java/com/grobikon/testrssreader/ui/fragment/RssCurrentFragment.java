package com.grobikon.testrssreader.ui.fragment;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.common.db.DBHelper;
import com.grobikon.testrssreader.common.manager.MyLinearLayoutManager;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.RssUrl;
import com.grobikon.testrssreader.model.view.BaseViewModel;
import com.grobikon.testrssreader.model.view.RssCurrentItemBodyViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.SimpleItemAnimator;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class RssCurrentFragment extends BaseRecyclerFragment{
    @Inject
    Realm realm;
    @Inject
    DBHelper dbHelper;
    private String[] rss;

    @BindView(R.id.et_rss)
    EditText etRSS;
    private SQLiteDatabase database;

    public RssCurrentFragment() {
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.lf_title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestRssReaderApp.getApplicationComponent().inject(this);

        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        readData();
    }

    private void initArray() {
        rss =  this.getResources().getStringArray(R.array.rss);
    }

    @OnClick(R.id.b_save_rss)
    public void saveRss() {
        if ( !"".equals(etRSS.getText().toString())) {
            saveNewUrlDB("test", etRSS.getText().toString());
            readData();
        }
    }

    @Override
    public void setUpRecyclerView() {
        MyLinearLayoutManager mLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void readData() {
        showListProgress();
        Cursor cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null, null);
        List<RssUrl> rssUrls = new ArrayList<>();
        if (cursor.getCount() == 0) {
            initArray();
            for (String s : rss){
                saveNewUrlDB("test", s);
            }
        }
        cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBHelper.KEY_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            int urlIndex = cursor.getColumnIndex(DBHelper.KEY_URL);
            do {
                RssUrl rssUrl = new RssUrl();
                rssUrl.setId(cursor.getInt(idIndex));
                rssUrl.setNameRss(cursor.getString(nameIndex));
                rssUrl.setRssUrl(cursor.getString(urlIndex));
                rssUrls.add(rssUrl);
            } while (cursor.moveToNext());
        }
        setItems(parsePojoModel(rssUrls));
        cursor.close();
        hideListProgress();
        hideRefreshing();
    }

    private List<BaseViewModel> parsePojoModel(List<RssUrl> rssUrls) {
        List<BaseViewModel> baseItem = new ArrayList<>();
        for (RssUrl s : rssUrls) {
            baseItem.add(new RssCurrentItemBodyViewModel(s, mAdapter));
        }
        return baseItem;
    }

    @Override
    public void setUpSwipeToRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(this::readData);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mProgressBar = getBaseActivity().getProgressBar();
    }

    private void saveNewUrlDB(final String name, final String url) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.KEY_NAME, name);
        contentValues.put(DBHelper.KEY_URL, url);
        database.insert(DBHelper.TABLE_CONTACTS, null, contentValues);
        etRSS.setText(null);
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_rss_current;
    }
}
