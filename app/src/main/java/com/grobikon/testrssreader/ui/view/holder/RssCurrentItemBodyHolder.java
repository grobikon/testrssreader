package com.grobikon.testrssreader.ui.view.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.common.SessionManager;
import com.grobikon.testrssreader.common.db.DBHelper;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.RssUrl;
import com.grobikon.testrssreader.model.view.RssCurrentItemBodyViewModel;
import com.grobikon.testrssreader.ui.activity.BaseActivity;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class RssCurrentItemBodyHolder extends BaseViewHolder<RssCurrentItemBodyViewModel> {
    @BindView(R.id.body)
    RelativeLayout rl;
    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.iv_delete)
    ImageView iv_delete;
    @Inject
    SessionManager sm;
    @Inject
    DBHelper dbHelper;
    private Context mCtx;

    public RssCurrentItemBodyHolder(View itemView) {
        super(itemView);
        mCtx = itemView.getContext();
        ButterKnife.bind(this, itemView);
        TestRssReaderApp.getApplicationComponent().inject(this);
    }

    @Override
    public void bindViewHolder(RssCurrentItemBodyViewModel item) {
        tvText.setText(item.getRssUrl().getRssUrl());

        currentTitle(item);

        itemView.setOnClickListener(v -> {
            sm.saveRSS(item.getRssUrl().getId() - 1);
            item.getAdapter().notifyDataSetChanged();
        });

        iv_delete.setOnClickListener(view -> {

            dbHelper.getWritableDatabase().delete(DBHelper.TABLE_CONTACTS, DBHelper.KEY_ID + "= " + item.getRssUrl().getId(), null);
            int pos = getAdapterPosition();
            item.getAdapter().removeItem(pos);
        });
    }

    private void currentTitle(RssCurrentItemBodyViewModel item) {
        int indexCurrent = sm.getRSSCurrent() + 1;
        if (item.getRssUrl().getId() == indexCurrent) {
            rl.setBackgroundColor(mCtx.getResources().getColor(R.color.colorBlackSecondaryTextWhite70));
        } else {
            rl.setBackgroundColor(mCtx.getResources().getColor(R.color.colorBlackGreyDialogBackground800));
        }
    }

    @Override
    public void unbindViewHolder() {
        tvText.setText(null);
    }
}
