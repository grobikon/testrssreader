package com.grobikon.testrssreader.ui.fragment;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.grobikon.testrssreader.R;
import com.grobikon.testrssreader.common.HTTPDataHandler;
import com.grobikon.testrssreader.common.SessionManager;
import com.grobikon.testrssreader.common.db.DBHelper;
import com.grobikon.testrssreader.di.TestRssReaderApp;
import com.grobikon.testrssreader.model.Item;
import com.grobikon.testrssreader.model.RSSObject;
import com.grobikon.testrssreader.model.view.BaseViewModel;
import com.grobikon.testrssreader.model.view.RssItemBodyViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class TestRssReaderFragment extends BaseRecyclerFragment {
    @Inject
    SessionManager sm;
    @Inject
    DBHelper dbHelper;
    //RSS link
    private String rssUrl ="https://www.vedomosti.ru/rss/news";
    private final String RSS_to_Json_API = "https://api.rss2json.com/v1/api.json?rss_url=";
    RSSObject rssObject;

    public TestRssReaderFragment() {
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.sf_title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestRssReaderApp.getApplicationComponent().inject(this);
        initUrlRss();
    }

    private void initUrlRss() {
        String selection = "_id = ?";
        String[] selectionArgs = new String[] {String.valueOf(sm.getRSSCurrent())};
        Cursor c = dbHelper.getWritableDatabase().query(DBHelper.TABLE_CONTACTS, null, selection, selectionArgs, null, null, null);
        if(c.moveToFirst()){
            rssUrl = c.getString(c.getColumnIndexOrThrow (DBHelper.KEY_URL));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadRSS();

    }

    private void initArray() {
    }

/*    private int getNextKey() {
        return getBaseActivity().getRealm().where(RssUrl.class).count() == 0 ? 0 : (getBaseActivity().getRealm().where(RssUrl.class).max("id")).intValue() + 1;
    }*/


    private void generatePhrase() {
        this.setResult();
    }

    private void setResult() {
    }

    private void loadRSS() {
        AsyncTask<String,String,String> loadRSSAsync = new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                showListProgress();
            }

            @Override
            protected String doInBackground(String... params) {
                String result;
                HTTPDataHandler http = new HTTPDataHandler();
                result = http.GetHTTPData(params[0]);
                return  result;
            }

            @Override
            protected void onPostExecute(String s) {
                rssObject = new Gson().fromJson(s, RSSObject.class);
                setItems(parsePojoModel(rssObject));
                hideListProgress();
                hideRefreshing();
            }
        };

        StringBuilder url_get_data = new StringBuilder(RSS_to_Json_API);
        url_get_data.append(rssUrl);
        loadRSSAsync.execute(url_get_data.toString());
    }

    private List<BaseViewModel> parsePojoModel(RSSObject rssObject) {
        List<BaseViewModel> baseItem = new ArrayList<>();
        for (Item i : rssObject.getItems()) {
            baseItem.add(new RssItemBodyViewModel(i, mAdapter));
        }
        return baseItem;
    }


    @Override
    public void setUpSwipeToRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(this::loadRSS);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mProgressBar = getBaseActivity().getProgressBar();
    }

}
